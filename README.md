## ASP .NET CORE 2.2 API Medley

This repository is a template for a quick start developing web API using various features, such as: 

1. Swagger documentation
2. Any CORS permission
3. MongoDB repository implementation
4. Controllers example (with images uploading) + client and server models
5. Excetptions and errors responses implementation
6. Identity server implementation for authorization and authentication
