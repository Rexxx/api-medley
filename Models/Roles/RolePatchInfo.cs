namespace Models.Roles
{
    public class RolePatchInfo
    {
        public string UserName { get; }
        public string UserRole { get; set; }

        public RolePatchInfo(string userName, string userRole = null)
        {
            UserName = userName;
            UserRole = userRole;
        }
    }
}