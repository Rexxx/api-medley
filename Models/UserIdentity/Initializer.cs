using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Models.Roles;
using Models.Users;

namespace Models.UserIdentity
{
    public static class Initializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            const string adminRole = "admin";
            const string userRole = "user";

            if (await roleManager.FindByNameAsync(adminRole) == null)
            {
                await roleManager.CreateAsync(new Role(adminRole));
            }
            if (await roleManager.FindByNameAsync(userRole) == null)
            {
                await roleManager.CreateAsync(new Role(userRole));
            }

            await RegisterUser(userManager, adminRole);
            await RegisterUser(userManager, userRole);
        }

        private static async Task RegisterUser(UserManager<User> userManager, string userRole)
        {
            const string email = "@lapkisoft.me";
            const string password = "qwe123";
            const string phoneNumber = "8-800-000-00-00";
            
            if (await userManager.FindByNameAsync(userRole) == null)
            {
                var dateTime = DateTime.UtcNow;
                
                var user = new User
                {
                    UserName = userRole,
                    Name = string.Empty,
                    Email = $"{userRole}{email}",
                    PhoneNumber = phoneNumber,
                    RegisteredAt = dateTime,
                    LastUpdateAt = dateTime
                };

                var result = await userManager.CreateAsync(user, password);
                
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, userRole);
                }
            }
        }
    }
}